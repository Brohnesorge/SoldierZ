HOW TO PLAY & WEAPON INFO

Thank you for downloading The Soldier Z! This is a brief little manual on how to play just in case you didn't read the OP or if I miss updating it. Which happens. A lot. I'm lazy. Anyway...

Ammo is magazine based. This means if you reload and still have ammo in your gun, it's gone. Exception is shells, for obvious reasons.

Make sure you have Alt-fire, Reload, Zoom, Weapon State 1 and Weapon State 2 all bound.
-Alt-fire's function changes from weapon to weapon, but it's usually ADS or working the action.
-Reload is...well, reloading! 
-Zoom is the main gimmick of the mod. It throws your current weapon away to quick draw your sidearm. Many reloads are quite long, and they can be interupted with this! You can't throw your SMG away, but can still quick draw.
-Weapon State 1 is used for throwing grenades. Grenades bounce, are affected by gravity, and explode against enemies.
-Weapon State 2 is used for misc functions, based on the weapon.

When you start the game, you get a choice of 3 classes: 1 of 3 SMGs. The two SMGs have different properties and starting equipment.
SMGs are...
-UMP45: Supressed, more ammo, 1 medikit, 1 grenade, blue armor. Hit them quiet and hard! Upgrades focus on making the gun even stronger.
-MP5K PDW: Less ammo, 2 medikits, 6 grenades, green armor. Loud, fast and explosive! Upgrades focus on grenades, and work across all weapons.
-P90: Big magazine, stable damage, 1 medikit, 9 Flashbangs and green armor.

You select what Sidearm you have in the options menu. Make sure you pick on before playing, and it defaults to the Jericho. Sidearms are...
-Pistol: Jericho 941. The weakest but quickest side arm so far. 16 round magazine, unlimited magazines.
-Heavy Pistol: MK23 SOCOM. Stronger, but slower. 12 round magazine and suppressed. Unlimited magazines.
-Revolver: Ruger Blackhawk. Very strong, must be manually cocked with Alt-Fire. 6 round magazine with a very slow reload. Unlimited ammo.
-Sawn Off. Uses your shotgun ammo and is double barrel. The SSG basically. The only side arm without unlimited ammo, but you start with shells.

Weapons are modeled after their real life equivilent, with some adjustment for gameplay's sake. I won't go into great detail here, just their buttons.
Some weapon spawns are adjustable, make sure you check out the options before playing!
-There is no chainsaw. Instead you find a random upgrade for a weapon you already have. I won't go into detail here about each upgrade. See the OP or find out through gameplay.
-SMGs Alt-fire ADS, Weaponstate 2 switches firemode between auto, burst and semi.
-Shotgun Alt-fire pumps, Weaponstate 2 unloads your shells and switches ammo type. Ammo types are buckshot, flechettes and slugs.
-SPAS-12 Weaponstate 2 switches firemode between semi auto and pump action. Alt-fire pumps in pump actions. Alt-fire unloads and switches ammo type in semi auto.
-AUG Alt-fire scopes, Weaponstate 2 fires the grenade launcher. Weaponstate 2 also reloads the grenade launcher if its empty.
-G36C Alt-Fire scopes. Weaponstate 2 does nothing.
-Minimi has no special functions.
-M79 has no alt-fire and no WeaponState2. 
-The LAW is a special weapon. Picking one up does nothing, intially. The LAW is added to your use item inventory, and must be used to be prepared. Once prepared, it can be fired before being thrown away and grabbing another one if more than one is prepared. You can carry up to 4 in reserve, and have up to 3 prepared. It has no special functions outside of this.
-Plasma Alt-fire fires an explosive charge shot. Zoom doesn't draw your pistol on this weapon, instead it cools it down. Weaponstate 2 switches firemode between Normal, Suppression and Scatter. Suppression rapid fires small bolts that force pain, and Scatter is a shot gun blast of these bolts.
-BFG has the same functions as the Plasma, minus the firemodes.
-Sniper Alt-fire pulls the bolt. Weaponstate 2 uses the Scope. You can pull the bolt while zoomed in.

Most items can now be picked up and used where ever.
-Medikits heal you, but not instantly. They slowly heal you over time. The higher your health the faster they heal. The lower your health, the more they heal. They will always heal at least 25 HP. Getting hit stops the healing, so only do it when you're safe.
-Ninjasphere turns you invisible and boosts your speed.
-Hazmat suit does what you think.
-Night Goggles do what you think, and are togglable. 
-4x Damage fully heals you instantly and you do quad damage for a short duration.
-Megaspheres overheal you 200, give you blue armor, make you invulnerable and give you quad damage all at once.
